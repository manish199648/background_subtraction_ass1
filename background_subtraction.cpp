#include "opencv2/opencv.hpp"
#include<iostream>
const int  ngaussian = 3 ;
const int  training_frames = 30 ;
const double new_var = 10000000000;
const double alpha = 0.002;

double weight[ngaussian];
 double threshold1 = 0.7;
 double temp;
 double mini;
 double cent[3];
 double data_p[3];
 double temp1[ngaussian][3];
 double cov_num[3][3];
 double mean_num[3];
 double likeli;
 double temp3[3];
 double rho;
using namespace cv;
using namespace std ;

double epsilon = 0.001; // for comparing double
class gaussian {

public:
	double mean[3];
	int dimension;
	double covariance[3][3];
	double val_cov;
	double pi;
	int id;
	gaussian()
	{
	}
	int getid() {
		return id;
	}
	gaussian(double data[3], double var, double tpi, int y)
	{
		id = y;
		dimension = 3;
		mean[0] = data[0];
		mean[1] = data[1];
		mean[2] = data[2];
		pi = tpi;  
		val_cov = 0;
		//cout << var << " " << "gaussian innit" <<pi <<" " <<endl;
		//cout << mean[0] << " "<<mean[1]<<" "<<mean[2] << "gaussian innit" << endl;
		// initialising value of probability

		double matrix[3][3];
		//covariance[3][3] = {{var,0,0},{0,var,0},{0,0,var}}	;
		for (int i = 0; i<3; i++)
		{
			for ( int j = 0; j<3; j++)
			{
				if (i == j)
					covariance[i][j] = var;
				else
					covariance[i][j] = 0;

				matrix[i][j] = covariance[i][j];
			}
		}
		val_cov = pow(var, 3);

		/*
		// calculation of mean
		for(int i=0 ;i <dimension ;i++)
		{	double temp =0 ;
		for(int j=0; j<size ;j++)
		{
		temp = temp + data[j][i] ;
		}
		mean[i] =temp/size ;
		}

		//calculation of covariance matrix
		for(int i= 0 ;i <dimension; i++)
		{
		for(int j=0 ;j<dimension;j++)
		{	double temp = 0 ;
		for(int k= 0;k<size ;k++)
		{
		temp = temp + (data[i][k]-mean[i])*(data[k][j]-mean[j]) ;
		}
		covariance[i][j] = temp /size ;
		}
		}
		*/
		//calculation of deteminant

		

		// There is a formula for square 3rd row matrices
		//determinant = pow() ;


		 
		
	}

	double finddist( double input[])
	{
		temp = 0; 
		for ( int i = 0; i < 3; i++)
		{
			temp = temp + pow(input[i] - mean[i], 2);
		}
		temp = sqrt(temp);
		return temp/3 ;    // not sure about it , 
	}
	double get_sigma()
	{
		return covariance[0][0];
	}
	double get_mean1()
	{
		return mean[0];
	}
	double get_mean2()
	{
		return mean[1];
	}
	double get_mean3()
	{
		return mean[2];
	}
	double getcovariance()
	{
		return val_cov;
	}
	double getpi()
	{
		return pi;
	}
	double getprob(double input[])
	{
		if (abs(val_cov)<epsilon)
		{
			//cout << "used it";
			if ((input[0] - mean[0]) < epsilon && (input[1] - mean[1]) < epsilon && (input[2] - mean[2]) < epsilon)
				return 1;
			else
				return 0;
		}
		{
			//calculation of inverse of covariance matrix
			cv::Mat A(4, 4,CV_64FC1);

			for ( int i = 0; i < 3; i++)
			{
				for ( int j = 0; j < 3; j++)
				{
					A.at < double >(i, j) = covariance[i][j];
				}
			}
			/*
			cout << "yo1"; 
			for (int z = 0; z < 3; z++)
			{
				for (int t = 0; t < 3; t++)
				{
					cout << A.at<double>(z, t)<<" ";
				}
				cout << endl;
			}
			*/

			cv::Mat inverted(4, 4, CV_32FC1);
			//invert(A, inverted, cv::DECOMP_SVD);
			inverted = A.inv(DECOMP_LU);
			
			//cout << "yo2";
			
			//cout << mean[0] << " " << mean[1] << " " << mean[2]; 
			//calculation of mahanalobis distance
			double array[3];
			for ( int i = 0; i<3; i++)
			{
				temp = 0;
				for ( int j = 0; j<3; j++)
				{
					temp = temp + (input[j] - mean[j])*inverted.at<double>(j+1, i+1);
					//cout << temp << " ";
				}
				array[i] = temp;
				//cout << endl << " ";
			}
			//cout << endl;
			double distance = array[0] * (input[0] - mean[0]) + array[1] * (input[1] - mean[1]) + array[2] * (input[2] - mean[2]);
			//cout << "distance" << val_cov << "hello guys"<<" "<< (exp(-distance / 2)) / (pow(2 * 3.14, 3 / 2)*sqrt(val_cov));
			return (exp(-distance / 2) )/ (pow(2 * 3.14, 3 / 2)*sqrt(val_cov));
		}

	}

	void update_mean(double a[])
	{
		for ( int i = 0; i<dimension; i++)
		{
			mean[i] = a[i];
		}
	}

	void update_covariance(double a[][3])
	{
		for (int  i = 0; i<dimension; i++)
		{
			for ( int j = 0; j<dimension; j++)
			{
				covariance[i][j] = a[i][j];
			}
		}
		

		
		// There is a formula for square 3rd row matrices
		val_cov = (covariance[0][0] * covariance[1][1] * covariance[2][2]) ;
		}
	void update_var(double inp)
	{
		covariance[0][0] = covariance[1][1] = covariance[2][2] = inp;
	}
	void update_pi(double p)
	{
		pi = p;
	}
};
class kmean1 {
public:
	int frames;
	double points[training_frames][3];
	int dimension;
	double clusters[5][3];
	int no_centers;
	int iterations;
	int centers[training_frames] ;
	double pi[ngaussian];
	// constructer
	kmean1(double data[][3])
	{
		iterations = 10;
		frames = training_frames;  //number of frames
		no_centers = ngaussian; //number of clusters
		dimension = 3;
		for ( int i = 0; i<frames; i++)
		{
			for ( int j = 0; j< dimension; j++)
			{
				points[i][j] = data[i][j];
			}
		}
		for (int i = 0; i<no_centers; i++)
		{
			pi[i] = 0;
		}
	}

	void run_kmean1()
	{
		int number = 0;
		for ( int i = 0; i<no_centers; i++)
		{
			//int number = rng.uniform(0, frames - 1);
			for ( int j = 0; j<dimension; j++)
			{
				clusters[i][j] = points[number][j];
			}
			number =number+5;
			//cout << number << endl;
		}

		for (int y = 0; y<iterations; y++)
		{
			expectation();
			maximisation();
		}
	}
	void expectation()
	{
		for ( int i = 0; i <frames; i++)
		{
			mini = 100000000;
			int p;
			for (int j = 0; j<no_centers; j++)
			{
				if (pow(points[i][0] - clusters[j][0], 2) + pow(points[i][1] - clusters[j][1], 2) + pow(points[i][2] - clusters[j][2], 2)<mini)
				{
					//if(j<10)
					//cout << pow(points[i][0] - clusters[j][0], 2) + pow(points[i][1] - clusters[j][1], 2) + pow(points[i][2] - clusters[j][2], 2) << " ";
					mini = pow(points[i][0] - clusters[j][0], 2) + pow(points[i][1] - clusters[j][1], 2) + pow(points[i][2] - clusters[j][2], 2);
					p = j;
				}
			}
			centers[i] = p;
			//cout <<p<<" ";

		}
		//cout << endl;
	}
	void maximisation()
	{
		int count[ngaussian];
		for ( int i = 0; i<ngaussian; i++)
		{
			temp1[i][0] = 0;
			temp1[i][1] = 0;
			temp1[i][2] = 0;
			count[i] = 0; 
		}
		for ( int i = 0; i < no_centers;i++)
		{ 
		for ( int j = 0; j<frames; j++)
		{
			if(i==centers[j])
			{ 
			temp1[centers[j]][0] = temp1[centers[j]][0] + points[j][0];
			temp1[centers[j]][1] = temp1[centers[j]][1] + points[j][1];
			temp1[centers[j]][2] = temp1[centers[j]][2] + points[j][2];
			count[i]++;
			}
		}
		}
		for ( int i = 0; i < no_centers; i++)
		{
			if (count[i] == 0)
			{
				int hello = 0;
				int c = 0;
				for ( int j = 0; j<i; j++)
				{
					if (abs(clusters[j][0] - clusters[i][0])<epsilon&&abs(clusters[j][1] - clusters[i][1])<epsilon&&abs(clusters[j][2] - clusters[i][2])<epsilon)
					{
						hello = hello + count[j];
						c++;
					}
					
				}
				c++;
				for ( int j = 0; j <= i; j++)
				{
					if (abs(clusters[j][0] - clusters[i][0])<epsilon&&abs(clusters[j][1] - clusters[i][1])<epsilon&&abs(clusters[j][2] - clusters[i][2])<epsilon)
					{
						count[j] = hello / c;
						clusters[i][0] = clusters[j][0];
						clusters[i][1] = clusters[j][0];
						clusters[i][2] = clusters[j][0];
					}
				}

			}
			else
			{
				clusters[i][0] = temp1[i][0] / count[i];
				clusters[i][1] = temp1[i][1] / count[i];
				clusters[i][2] = temp1[i][2] / count[i];
			}
			//cout << clusters[i][0] << " " << clusters[i][1] << " " << clusters[i][2] << endl;
		
			//cout << count[0] << " " << count[1] << " " << count[2] << endl;
		}

		for ( int i = 0; i < ngaussian; i++)
		{
			pi[i] = count[i];
		}
		/*	for (int int i = 0; i<no_centers; i++)
		{
			//cout << count[i] << endl; 
			
			clusters[i][0] = temp[i][0] / count[i];
			clusters[i][1] = temp[i][1] / count[i];
			clusters[i][2] = temp[i][2] / count[i];
			//cout << clusters[i][0] << " " << clusters[i][1] << " " << clusters[i][2] << endl;
		}
		*/
	}
	double getcenter1(int i)
	{
		//cout << clusters[i][0]<<endl ; 
		return clusters[i][0];
	}
	double getcenter2(int i)
	{
		//cout << clusters[i][0]<<endl;
		return clusters[i][1];
	}
	double getcenter3(int i)
	{
		//cout << clusters[i][0]<<endl;
		return clusters[i][2];
	}
	void initpi()
	{

		for ( int i = 0; i<frames; i++)
		{
			pi[centers[i]]++;
		}

	}
	double getpi(int k) //kth model
	{
		return pi[k]/training_frames;
	}
	double variance(int k)   //kth model
	{
		 temp = 0;
		int si = 0;    //number of frames belonging to that cluster
		for ( int i = 0; i <frames; i++)
		{
			if (centers[i] == k)
			{
				temp = temp+(pow(points[i][0] - clusters[centers[i]][0], 2) + pow(points[i][1] - clusters[centers[i]][1], 2) + pow(points[i][2] - clusters[centers[i]][2], 2)) / 3;
				si++;
			}
		}
		//cout <<"variance of kth gaussian" <<k<<" "<<temp << " " << si << " ";
		if (si == 0)
		{
			return 0;   //to be taken care of what to be returned
		}
		else
		{
			return temp / si;
		}
	}
};

/*int waytosort1(timepass *lm , timepass *b)
{
	return 1;
}
*/
class pixels {

public:
	gaussian array[ngaussian];    					//modelling each pixel at max 5 gaussian
	double values[training_frames][3];   			//values of 1st n pixels
	double responsibility[training_frames][ngaussian];
	int ng;						//number of gaussian
	int tau; 						//number of training frames
	double likelihood;
	pixels()
	{
		likelihood = 0;
		ng = ngaussian;
		tau = training_frames;
	}
	void setpixelvalue_g(int ta, double intensity)
	{
		values[ta][1] = intensity;
	}
	void setpixelvalue_r(int ta, double intensity)
	{
		values[ta][0] = intensity;
	}
	void setpixelvalue_b(int ta, double intensity)
	{
		values[ta][2] = intensity;
	}
	void expectation_max()
	{
		kmean1 algo1(values);
		algo1.run_kmean1();
		//cout << "hi";
		//algo1.initpi();
		// random error could not figure out
		int q;
		for (q = 0; q < ng; q = q + 1)
		{
			cent[0] = algo1.getcenter1(q);
			cent[1] = algo1.getcenter2(q);
			cent[2] = algo1.getcenter3(q);
			gaussian temp2(cent, algo1.variance(q), algo1.getpi(q), q);
			
				array[q] = temp2;
		}
		int iter = 0;
		while (true)
		{
			//cout << "Iteration" << endl;
			expectation();
			maximisation();
			if (check_likelihood())
			{
				if (iter == 0)
				{
				}
				else
					break;
			}
			if (iter > 3)
				break;
			iter++;
		}
	}
	void expectation()
	{
		for ( int i = 0; i < tau; i++)
		{
			//cout << "expectation" << endl; 
			double deno = 0;
			
			data_p[0] = values[i][0];
			data_p[1] = values[i][1];
			data_p[2] = values[i][2];

			for ( int j = 0; j < ng; j++)
			{
				deno = deno + array[j].getpi()*array[j].getprob(data_p);
				//cout << array[j].getprob(data_p) <<"   obtaining prob   ";
			}

			//cout << deno << " " << i << endl;
			//char a;
			//getc(a); 
			//int g; cin >> g;


			for ( int j = 0; j < ng; j++)
			{
				responsibility[i][j] = array[j].getpi()*array[j].getprob(data_p) / deno;
				//cout << responsibility[i][j]<<" "<<endl;
			}
			//cout << endl;
		}
	}
	void maximisation()
	{
		for ( int i = 0; i < ng; i++)
		{
			//cout << "maximisation" << endl;
			double Nj = 0;
			mean_num[0] = 0;
			mean_num[1] = 0;
			mean_num[2] = 0;

			for (int f = 0; f < 3; f++)   //setting cov =0 ; 
			{
				for ( int j = 0; j < 3; j++)
				{
					cov_num[f][j] = 0;
				}
			}
			//cout << "i m here";
			for ( int j = 0; j < tau; j++)    //calculation of fraction of number of points
			{
				Nj = Nj + responsibility[j][i];
			}
			//cout << "i reached"; 
			for ( int j = 0; j < tau; j++)   //calculation of new mean
			{
				mean_num[0] = mean_num[0] + values[j][0] * responsibility[j][i];
				mean_num[1] = mean_num[1] + values[j][1] * responsibility[j][i];
				mean_num[2] = mean_num[2] + values[j][2] * responsibility[j][i];
			}
			mean_num[0] = mean_num[0] / Nj;
			mean_num[1] = mean_num[1] / Nj;
			mean_num[2] = mean_num[2] / Nj;
			//cout << mean_num[0] << " " << mean_num[1] << " " << mean_num[2] << endl;
			for ( int j = 0; j < tau; j++)
			{
				for (int p = 0; p < 3; p++)
				{
					for (int k = 0; k < 3; k++)
					{
						if (p == k)
							cov_num[p][k] = cov_num[p][k] + (values[j][p] - mean_num[p])*(values[j][k] - mean_num[k])*responsibility[j][i] / Nj;
					}
				}
			}
			double l = (cov_num[0][0] + cov_num[1][1] + cov_num[2][2]) / 3;
			cov_num[0][0] = cov_num[2][2] = cov_num[1][1] = l;
			//cout << "variance " << l << endl;
			//cout << "pij " << Nj / tau << endl;
			//updating new mean variance and pij
			array[i].update_mean(mean_num);
			array[i].update_covariance(cov_num);
			array[i].update_pi(Nj / tau);
		}
	}
	bool check_likelihood()
	{
		//cout << "in likelihood";
		 likeli = 0;
		for ( int i = 0; i < tau; i++)
		{
			 temp = 0;

			for ( int j = 0; j < ng; j++)
			{
				temp = temp + array[j].getpi()*array[j].getprob(*(values + i));
			}
			likeli = likeli + log(temp);
		}

		//cout << likeli << " "; 
		//int f;
		//cin >> f;
		if (abs(likeli - likelihood) < 0.05)
		{

			likelihood = likeli;
			//cout << likelihood << " " << endl;
			return true;
		}
		else
		{
			likelihood = likeli;
			//cout << likelihood << " ";
			return false;
		}
	}

	int check_belong(double input[])
	{
		for ( int i = 0; i < ng; i++)
		{
			/*double b = array[i].get_sigma();
			double temp4[3];
			a[0] = array[i].get_mean1(); 
			a[1] = array[i].get_mean2();
			a[2] = array[i].get_mean3();
			double t[3];
			t[0] = input[0]; 
			t[1] = input[1];
			t[2] = input[2];*/
			if (array[i].finddist(input) < sqrt(array[i].get_sigma())*2.5)
			{
				return i;
			}
		}
		return 10;
	}
	void remove_gaussian(double input[])
	{
		int least = 10;
		 temp = 1000000000000000;
		for ( int i = 0; i < ng; i++)
		{
			if (array[i].get_sigma() == 0)
			{
				least = i; break;
			}
			else if (array[i].getpi() / sqrt(array[i].get_sigma()) < temp)
			{
				least = i;
			}
		}

		array[least].update_mean(input);
		array[least].update_var(new_var);
		array[least].update_pi(0.01);
		double sum = 0;
		for ( int i = 0; i < ng; i++)
		{
			sum = sum + array[i].getpi();
		}
		for ( int i = 0; i < ng; i++)
		{
			array[i].update_pi(array[i].getpi() / sum);
		}
	}
	void learn(int k, double pix[])
	{
		for ( int i = 0; i < ng; i++)
		{
			if (i == k)
			{
				array[i].update_pi((array[i].getpi()*(1 - alpha)) + alpha);
			}
			else
			{
				array[i].update_pi((array[i].getpi()*(1 - alpha)));
			}
			 rho = alpha*array[i].getprob(pix);
			
			
			temp3[0] = (1 - rho)*array[i].get_mean1() + rho*pix[0];
			temp3[1] = (1 - rho)*array[i].get_mean2() + rho*pix[1];
			temp3[2] = (1 - rho)*array[i].get_mean3() + rho*pix[2];

			temp = pow(pix[0] - array[i].get_mean1(), 2) + pow(pix[1] - array[i].get_mean2(), 2) + pow(pix[2] - array[i].get_mean3(), 2);
			temp = rho*temp / 3;
			array[i].update_var((1 - rho)*array[i].get_sigma() + temp);
			array[i].update_mean(temp3);
		}
	}

	bool check_back(int k)
	{
		int id[ngaussian];
		for ( int i = 0; i < ng; i++) {
			id[i] = array[i].getid();
			weight[i] = (array[i].getpi()) / sqrt(array[i].get_sigma());
		}
		int l;
		bool ans = false;
		for ( int i = 0; i < ng; i++) {
			for ( int j = 0; j < ng - i - 1; j++) {
				if (weight[j] > weight[j + 1]) {
					temp = weight[j];
					weight[j] = weight[j + 1];
					weight[j + 1] = temp;
					l = id[j];
					id[j] = id[j + 1];
					id[j + 1] = l;
				}

			}
		}
		temp = 0;
		for (int i = ng - 1; i >= 0; i--)
		{
			temp = temp+ array[id[i]].getpi();
			if (id[i] == k)
				ans = true;
			if (temp > threshold1)
			{
				break;
			}
		}
		return ans;

	};

};
int width ;
int height ;



int main(int, char** argv)
{
     //reading video 
    VideoCapture capture("umcp.mpg"); // open the default camera
    if(!capture.isOpened())  // check if we succeeded
        return -1;
     
    width = capture.get(CV_CAP_PROP_FRAME_WIDTH) ;
    height = capture.get(CV_CAP_PROP_FRAME_HEIGHT) ;	
	//width = 160;
	//height =180; 
	cout<<width<<" "<<height;    
 pixels model[240][352] ;	// For NEW VIDEO, CHANGE THIS
    int tau =0 ;
	Mat background;
	Mat newframe;
	Mat frame;
	Mat frame2;
	Mat med; 
	Mat med2;
	Vec3b intensity;
	double pix[3];
	for (;;)
	{
		
		
		capture >> frame; // get a new frame from camera
		//fr[tau] = frame; 
		
		background = frame.clone();
		newframe = frame.clone();
		med = frame.clone();
		med2 = frame.clone();
		if (tau < training_frames)
		{
			for (int i = 0; i < height; i++)
			{
				for ( int j = 0; j < width; j++)
				{
					//cout << "Reading training data"<<endl; 
					 intensity = frame.at<Vec3b>(i, j);
					if (i == 0 && j == 0)
						cout << "Reading  Data for "<<tau<<" frame"<<endl;
					model[i][j].setpixelvalue_g(tau, intensity.val[1]);
					model[i][j].setpixelvalue_r(tau, intensity.val[0]);
					model[i][j].setpixelvalue_b(tau, intensity.val[2]);
					
				}
			}
			tau++;
		}
		else if (tau == training_frames)
		{ 
			cout << "Trainging started"<<endl; 
			//model[150][200].expectation_max();
			for ( int i = 0; i < height; i=i+2)
			{
				for ( int j = 0; j <width ; j=j+2)
				{
					

					model[i][j].expectation_max();
				}
				cout << i <<  " " << endl;
				if (i == 230) {
					cout << "Reached 230th frame out of 240" << endl;
				}
			}
			
			
			tau++;
		}
	
	else
	{
		//capture.release();

		//int  a = 1;
		

				
				 
		//cout << "Started testing" << endl;
			for ( int i = 0; i < height; i=i+2)
			{
				for ( int j =0; j < width; j=j+2)
				{
					 intensity = frame.at<Vec3b>(i, j);
					
					pix[0] = intensity.val[0];
					pix[1] = intensity.val[1];
					pix[2] = intensity.val[2];
					int match = model[i][j].check_belong(pix);
					if (match == 10)
					{
						model[i][j].remove_gaussian(pix);
						
						intensity.val[0] = 0;
						intensity.val[1] = 0;
						intensity.val[2] = 0;
						newframe.at<Vec3b>(i, j) = intensity;
						newframe.at<Vec3b>(i+1, j+1) = intensity;
						newframe.at<Vec3b>( i, j + 1) = intensity;
						newframe.at<Vec3b>(i + 1, j ) = intensity;
						intensity.val[0] = 0;
						intensity.val[1] = 0;
						intensity.val[2] = 0;
						background.at<Vec3b>(i, j) = intensity;
						background.at<Vec3b>(i + 1, j + 1) = intensity;
						background.at<Vec3b>(i, j + 1) = intensity;
						background.at<Vec3b>(i + 1, j) = intensity;
						
					}
					else
					{

						if (model[i][j].check_back(match))
						{
							
							intensity.val[0] = 255;
							intensity.val[1] = 255;
							intensity.val[2] = 255;
							newframe.at<Vec3b>(i, j) = intensity;
						 newframe.at<Vec3b>(i+1, j+1) = intensity;
							newframe.at<Vec3b>(i, j + 1) = intensity;
							newframe.at<Vec3b>(i + 1, j) = intensity; 
						}
						else
						{
							
							intensity.val[0] = 0;
							intensity.val[1] = 0;
							intensity.val[2] = 0;
							newframe.at<Vec3b>(i, j) = intensity;
							newframe.at<Vec3b>(i+1, j+1) = intensity;
							newframe.at<Vec3b>(i, j + 1) = intensity;
							newframe.at<Vec3b>(i + 1, j) = intensity; 
							intensity.val[0] = 0;
							intensity.val[1] = 0;
							intensity.val[2] = 0;
							background.at<Vec3b>(i, j) = intensity;
							background.at<Vec3b>(i + 1, j + 1) = intensity;
							background.at<Vec3b>(i, j + 1) = intensity;
							background.at<Vec3b>(i + 1, j) = intensity;
							
						}
						model[i][j].learn(match, pix);
					}

				}

			}	
			medianBlur(newframe, med, 3);
			medianBlur(background, med2, 3);
			imshow("Foreground", med);
			imshow("Video", frame);
			imshow("Background", med2);
			
		tau++;
	}	
		//cout <<endl <<tau<< " ";
	//imshow("yo", frame);
		if (waitKey(30) >= 0) break;

	}

	 
	 return 0;
}